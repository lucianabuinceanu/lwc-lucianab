import { api, LightningElement,track,wire} from 'lwc';
import {ShowToastEvent} from "lightning/platformShowToastEvent";
import { getRecord, getFieldValue } from "lightning/uiRecordApi";

export default class My_Test_Component extends LightningElement {
    @track currentOutput;
    input;
    @api
    recordId;
    input;
    

    inputChangeHandler(event)
    {
       this.input= event.target.value;
    }

    ValidateHandler()
    {
        const event=new ShowToastEvent({
            title: "Success!",
            message: "Valid SKU of product: "+(this.input),
            variant:"success"
    
        });
    this.dispatchEvent(event);
     
        }
    }
       /*
        
        {
            const event=new ShowToastEvent({
                title: "Failed!",
                message: "Invalid SKU of product: "+(this.principal),
                variant:"error" 
            });
        this.dispatchEvent(event);
        }
    }
    */

    